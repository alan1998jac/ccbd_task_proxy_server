No of group members : 1

Name : Alan Jacob

USN : 01FB16ECS050

mail ID: alan1998jac@gmail.com

The task I have chosen is the first one (to create multiple proxies to espn cric info)

• Task Objective:
• You will build a proxy web-site that serves out web pages of an IPL match.
•
You will set the proxy server of your web browser to point to the address of the
web server that you will deploy.
• On the first request to a web-page, you will download the page from the
espncricinfo site in a cache.
• On subsequent requests you will return it from your cache.
•
In order to work with heavy loads, your web server will use two or more proxy
servers and share the load amongst these in round-robin order; meaning that
all the servers are assigned numbers from 0, 1, 2...n in a list and the next
server on the list will respond to the request.
• Design challenges
• How to cache the shared data?
• Is there any alternative to round-robin algorithm?


Solution:

Tools used : Nginx
 
servers created using: EC2 instance of aws

In the task I am supposed to  create multiple proxies such that each successive request from the client goes to a specific proxy server in round robin fashion.
NGINX by default uses round robin algorithm.

For this I have created three ubuntu 16.04 lts server on aws, one of the servers serves as the load balancer and the remaining two are the proxies for espncricinfo.

For server I have used nginx.

First I connect to each of the servers via ssh and install nginx on all the three instances.
The two proxies contain the espncricinfo pages in the location /etc/www/html , nginx serves pages present at this location.

The second step is to alter the default nginx.conf file on the load server so that it passes on the client request to the proxy in round robin fashion.


Alternate approach to round robin:

1)least connections: in this approach the load balancer routes the client request to the proxy server having least no of
       				 active connections.

       				 
2)ip hash: in this approach the load balancer converts the ip address of a client to a hash value and then it determines   
           which proxy server the client request goes to. In this approach a specific ip address always maps to a specific
           proxy sever as the ip address of the client converts to a hash value 